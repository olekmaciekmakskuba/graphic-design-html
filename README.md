Nauczyciel:
Zalogowany nauczyciel widzi listę utworzonych przez siebie ćwiczeń.
Ma możliwość utworzenia nowego ćwiczenia.
Widok tworzenia ćwiczenia:
nauczyciel powinien wypełnić metadane - tytuł, opis, wybór języka programowania (ew. programu budującego - maven/gradle/..)
koniecznym jest przygotowanie skryptu budującego - możliwość dostarczenia nauczycielowi szablonu
stworzenie szablonu ćwiczenia dla ucznia, możliwy import paczki zip
Uczeń:
uczeń widzi listę zaczętych zadań, może przejść do jego kontynuacji
uczeń widzi listę rozwiązanych zadań, może zobaczyć szczegóły rozwiązania - feedback w postaci relacji z testów
Gość (każdy użytkownik):
widzi listę wszystkich zadań (tytuł, język) - filtry (język, wyszukiwanie po tytule/opisie)
w szczegółach zadania widzi dodatkowo jego opis, pojawia mu się Button rozwiąż (jeżeli użytkownik nie jest zalogowany, przekierowuje go na stronę logowania)
info o plikach dostarczających czyli paczkę
zalogowany użytkownik ma to co nauczyciel + uczeń